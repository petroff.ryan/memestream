// This file is to enable to linux-browsersource to play .webm files without an html shim
document.getElementsByTagName("video").media.controls = false;
// it works! Controls are successfully hidden, webm successfully autoplay at full zoom! ^____^

document.getElementsByTagName("body")[0].style.background = "transparent";
// this makes backgrounds transparent, really nice!

// Now we just need to make the browser close or hide when playback finishes.
document.getElementsByTagName("video")[0].onended = function () {document.getElementsByTagName("video")[0].hidden = true;}
