var http = require('http')
var url = require('url')
var fs = require('fs')
var path = require('path')
var baseDirectory = __dirname // or whatever base directory you want

var ip = require("ip");
var port = 8080

console.log("Logging with console.log works :)");

console.log("Try " + "http://" + ip.address() + ":" + port + "/index.html");

http.createServer(function(request, response) {
  try {
    var requestUrl = url.parse(request.url)
    console.log(requestUrl);

    if (request.method == 'POST') {
      console.log('POST')
      console.log(request.url)
      userAction(request.url);
      var body = ''
      request.on('data', function(data) {
        body += data
        //console.log('Partial body: ' + body)
      })
      request.on('end', function() {
        //console.log('Body: ' + body)
        //response.writeHead(200, {'Content-Type': 'text/html'})
        //response.end('post received')
      })
    }



    // need to use path.normalize so people can't access directories underneath baseDirectory
    var fsPath = baseDirectory  + path.normalize(requestUrl.pathname)

    console.log(fsPath)

    var fileStream = fs.createReadStream(fsPath)
    fileStream.pipe(response)
    fileStream.on('open', function() {
      response.writeHead(200)
    })
    fileStream.on('error', function(e) {
      response.writeHead(404) // assume the file doesn't exist
      response.end()
    })
  } catch (e) {
    response.writeHead(500)
    response.end() // end the response so browsers don't hang
    console.log(e.stack)
  }
}).listen(port)

// console.log("listening on port "+port)

const OBSWebSocket = require('obs-websocket-js');

const obs = new OBSWebSocket();

obs.connect({
    address: 'localhost:4444',
    password: 'password'
  }).then(() => {
    console.log(`Success! OBS connected & authenticated.`);

    return obs.send('GetSceneList');
  })
  .then(data => {
    console.log(`${data.scenes.length} Available Scenes`);
    console.log(data);
  })
  .catch(err => { // Promise convention dicates you have a catch on every chain.
    console.log(err);
  });

obs.on('SwitchScenes', data => {
  console.log(`New Active Scene: ${data.sceneName}`);
  // console.log(data);
});

// You must add this handler to avoid uncaught exceptions.
obs.on('error', err => {
  console.error('socket error:', err);
});

var userAction = function(memehandle) {
  console.log('userAction triggered!', memehandle)

  /*
    obs.send('GetCurrentScene')
    .then(data => {
        console.log(data);
    })
    .catch(err => { // Promise convention dicates you have a catch on every chain.
        console.log(err);
    });
  */

  /*
    obs.send('GetSourceSettings', { sourceName: 'jswsVideoSource' })
    .then(data => {
        console.log(data);
    })
    .catch(err => { // Promise convention dicates you have a catch on every chain.
        console.log(err);
    });
  */

  obs.send('SetSourceSettings', {
      sourceName: 'jswsVideoSource',
      sourceSettings: {
        close_when_inactive: true,
        local_file: baseDirectory + '/memes' + memehandle + '.webm'
      }
    })
    .then(data => {
      console.log(data);
    })
    .catch(err => { // Promise convention dicates you have a catch on every chain.
      console.log(err);
    });

}
