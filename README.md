# MEMESTREAM

## Overlay choice memes on your video streams
### OBS + memes + server + touchscreen controller (eg raspi, tablet) = Victory


Using websockets as documented here: https://github.com/Palakis/obs-websocket/blob/4.x-current/docs/generated/protocol.md

We'll start in JS using this: https://github.com/haganbmj/obs-websocket-js

We npm init, npm install obs-websocket-js, and we start trying to work with node<->OBS.

Connection established!

We want the code to add a single meme ovelay, then remove it cleanly.

So we need to:
[ ] Get the current state
[ ] Add a new source meme
[ ] Remove the meme when it finishes playing
[ ] Verify state is same as it was

We start with the examples provided.

The examples... error out... Today may be more challenging than anticipated.
The node example runs with many errors on its own, although not when copy-pasted. It has some extra dependencies?

The samples were written with v1.2, I'm using the latest v3.0, so perhaps some friction is to be expected.

So! We were trying to get the current state?
Though the sample seems to error out on an authentication failure, it actually connects correctly and logs the current scene on scene changes, which is good for what we want.

Something's messed up. obs.onSwitchScenes works in samples, but not in my code? It's not a function? Why?

There's a different flavour of example in the README, so let's give that a shot.
Aha! Trust the README is the rule here.
If we are feeling very generous at the end of this, we might make a pull request to fix those old examples. Maybe.

Oh, jeeze, JS has so much going on these days. Promises and template stings, I guess that's just what's up now.

We're getting useful data now. Let's try to push our luck.

So, step one was 'get the current source'.
There's some 'debug' functionality that needs some more dedication. We ignore it for now, since after 3 tries it won't work how I expect.

So, we are getting the data from OBS, and we've got the current-scene. Good so far!

So, step one was get the data, and step two is add a source.
We consult the protocol (and we note that later we'll want to use 'heartbeat') at
(will we need GetFilenameFormatting? GetSceneItemProperties? SetSceneItemProperties? SetSourceSettings?)

It seems there may be no AddSource function available? That's surprising.
We'll need to play a source there and just change its properties instead of creating/deleting per meme.
Let's make sure that's really true before we move on.
Is there really no 'add source' in the websocket functions?

Aww jeeze you guys: https://github.com/Palakis/obs-websocket/issues/179
It looks like I'm not the only one who wants this.
Maybe... maybe we're going to make a pull request?
This seems like it'll be a SERIOUS deep-dive. We'll see.

Well, that's not happening today, so let's try it the hacky and available way: toggling and switching an existing source.
We create a new scene for testing now...
TESTjsws has two sources, my webcam and jswsVideoSource.

We need our JS script to accept some user input at the terminal where we're testing it.
We find the npm 'keypress' package, what should be an obvious winner.
We cross our fingers and bow our head and say a silent prayer.
Will it work?
----
We took a break for dinner, and we return FRESH.
We find that the keypress library is not going to be what we want.
We search for alternatives.

Let's try iohook. We npm install and we try.
It works, but perhaps too well - it captures EVERYTHING, not just the window I want.

Let's swap that out, doesn't feel right. Feels insecure. Even for testing.

We try a code snippet from stackoverflow, and it errors. We investigate.
Investigation shows that the features this code relies on have been REMOVED from the language/runtime. OK.
The search continues? This should be so simple, right?
Keypresses?!


We try another snippet.
Ayyyyyy! This one works! Nice!

It traps the terminal, making it unexitable. Nice.
Let's splice in a safety.

mmmm we keep working at it until it exits clean. A few more tries should get us there.

Got it! Fifth try's the charm. :P

So!
Now we've got interactive user input available! Nice!

We nice things up a bit and have a clean working solution. Committed!

----

We took a break for hydration.

Here's what I want this time:

1) load up, connect to OBS
2) query current scene, scene's sources
3) find our special jswsVideoSource
4) change that source's input file

If that causes it to play, correctly, once - huge win. Will it work right the first time?  

1, done.
2, getting current scene, need scene's sources.

We're going to do all of this inside our new user-interaction-keypress function.

so 2 will now be:

2) On keypress, get current scenes and sources, change our special source's input file.

We have been working in nano in a bunch of terminals, but it's time to graduate to Atom.

We do so - see you on the other side.

-----


Well, that's a bit more comfortable.
Not as pure, but let's get back to work. :)

We use promises and GetSceneList, and that's what we get on spacebarpress. So far so good.

Next, we need the sourcelist for the current scene.

GetSceneItemProperties ?

Let's experiment with using DuplicateSceneItem.

and start with GetCurrentScene. (a winner!)
Hmm, we don't get the filename.
Actually, are we able to get the filename?
If we can't, we might need to
trigger an OBS script.
Let's check if we can do THAT.
lets check GetSourceSettings ?

It was just about having the case-correct true name of the request parameters.

It gives the video file path. Could this be it?
We try to set instead of simply get.


AYYYYYYY!!!!!! IT WORKS!!!!!!!!

Setting it once makes it play once and end!
It's perfect!!!!!!!

It seems to... build up audio/video desync, with audio becoming MASSIVELY delayed and strange.

We'll go on a bughunt after a touch of celebration.

Restarting OBS makes it all better.
Even self-interruption is working right!!!
JOY!

Now that I know I have everything I need,
I'm going to go back to my design sketches, and pick out exactly the right frontend.

Since there's a nice vue thing made already, I'll probably... learn vue also?

----

Took a break to eat and think.

I'm going to learn vue as part of this arc.
Seems worthwhile.

So we set forth on the vue site!

Just watching the into video has made me wail out NOOOOOOOOOOOO more than once.
There's 'magic' and there's unclear mixing of html+js, and I'm like nNnnnNoooooooooOOOOOOOO0000O!!!!

BUT!
But we'll get past that.
Deep breaths.

Whoooooo
That's better. Back to the video.

Vue has a special chrome plugin....
Something tells me they didn't make an equally powerful firefox plugin.

bleh, but whatever, let's tuck in.
I don't think any more code is getting written today (It's after midnight, but you know what I mean) so some educational video content seems like it could be okay.
Let's try https://www.vuemastery.com/courses/intro-to-vue-js/vue-instance/

There is a firefox add-on, so I install it.
I feel a little better already.
Let's test the tooling out on t2t2's OBS remote!

uhhhhh
The vue dev tools panel does not actually... appear.... and it's not just me......... https://github.com/vuejs/vue-devtools/issues/366

I restart firefox, and take a deep breath.
Or two.
lol yup,
Just needed to restart firefox.

Turn it off and turn it back on again, now the correct solution to several different problems today!

We learn that Pascal Case is camelCase but UpperCamelCase. See? All cappy, that's PascalCase.

-------

We open a new chapter in this project!~

Everything is set up and approximately working, including the touchscreen emplacement.

What's missing is pressable buttons on the frontend! The current MVP solution of img-on-button has a funny side-effect because of the touchscreen: presses are accidentally drags!

We can obviously make things right, right now.

We're going to make a simple grid of six buttons that fill the screen.

The screen is in a landscape orientation and will be divided in 2 rows and 3 columns.

Thumbnails of the memes will be the backgrounds of these buttons, and their 'slug' or name or text will be centered in each button, black text with a white outline.

Once the frontend feels better, we'll fix the backend script up a bit, and then re-re-re-encode the meme video files to see if that eliminates the errors and sound-delay bug (which is pretty showstoppingly bad).

So! A frontend!
Let's remember not to get too fancy or be too heroic - we're not throwing an undigested understanding of vue at it today - the video playback doesn't work right, it's the wrong priority.

We just want something that approximately works.
That's just six buttons.
That also means six memes:
One "Huh?" a la DHMIS.
and one yet undecided "success" meme, to be chosen at the right time.

We go to edit... sigh, "gay.html"
I guess the first thing is to rename it index.html.
:P :) ^____^

We move a whole buncha stuff around.

Okay, everything is cleaned up and squared away a bit more.

Now we need to make this html sing.

 ----

 We learn a bunch about new css, that's cool.

 We still struggle a bit trying to get '100% height' grid things...

 We're going to cheat and just use the raspi touchscreen resolution.
 We'll try for... five more minutes before doing that.

Found it! it's the never-heard-of unit 'vh'
for view height, i guess

height: 100vh is the right thing for 100% tall! nice! Thanks, internet!

This is looking ready to test on the touchscreen! let's git push and go to the streamputer! ^____^!

----


This hasnt been kept updated.
Things are all changed now. Whoops.
